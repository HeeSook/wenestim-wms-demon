package com.wenestim.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.wenestim.db.DBManager;

/**
 * @author Andrew
 *
 */
public class AppUtil
{

	final static Logger LOGGER = Logger.getLogger(AppUtil.class);

	private static Properties  properties     = new Properties();
	private static InputStream input          = null;
	private static String      propertyValue  = "";
	private static String      filename       = "wenestim.wms.properties";
	public  static DBManager   dbManager      = new DBManager();
	private static String      hostAddress;

	public AppUtil()
	{
		// TODO Auto-generated constructor stub
	}

	public static String getPropertyByName(String propertyName) throws IOException
	{

		try {
			input = AppUtil.class.getClassLoader().getResourceAsStream(filename);
			if(input == null)
			{
				LOGGER.warn("Error Occurred, Unable to find " + filename);
	            return null;
			}

			//load a properties from inside of property file
			properties.load(input);

            //get the property value
			propertyValue = properties.getProperty(propertyName);

		}
		catch (IOException e)
		{
			LOGGER.error("IOException: " + e.getMessage());
		}
		finally
		{
			if(input!=null)
			{
        		try
        		{
        			input.close();
        		}
        		catch (IOException e)
        		{
        			LOGGER.error("IOException: " + e.getMessage());
        		}
        	}
		}

		return propertyValue;
	}

	/**
	 * get character position
	 *
	 * @param str
	 * @param searchChar
	 * @return
	 */
	@SuppressWarnings("unused")
	private static ArrayList<String> getCharPosition(String str, char searchChar)
	{
        ArrayList<String> positions = new ArrayList<String>();

        if (str.length() == 0)
        {
        	return null;
        }

        for (int i = 0; i < str.length(); i ++)
        {
            if (str.charAt(i) == searchChar)
            {
                positions.add(String.valueOf(i));
            }
        }

        return positions;
	}

	public static void exit(String errMsg)
	{
		LOGGER.error(errMsg);
        System.exit(-1);
	}

	public static void setHostAddress(String hAddress)
	{
		hostAddress = hAddress;
	}

	public static String getHostAddress()
	{
		return hostAddress;
	}

}

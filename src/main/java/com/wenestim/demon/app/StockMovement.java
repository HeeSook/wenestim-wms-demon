package com.wenestim.demon.app;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;

import com.wenestim.util.AppUtil;

/**
 * @author Andrew
 *
 */
public class StockMovement
{
	private final static Logger LOGGER = Logger.getLogger(StockMovement.class);


	public StockMovement()
	{

	}

	public int createStockMovement()
    {
		LOGGER.info("WeNestIm WMS createStockMovement Start");
		List<HashMap<String,Object>> actualList = null;
		int i         = 0;
		int insertCnt = 0;
		try
    	{
			actualList = AppUtil.dbManager.selectList("movement.getMesActualList");

			for(HashMap<String,Object> actualMap : actualList)
			{
				try
				{
					i++;
					LOGGER.info("insert Index : "+i);
					String clientId    = "";
					String companyCode = "";
					JSONObject jsonObject = new JSONObject();
					for ( String key : actualMap.keySet() )
					{
//						LOGGER.debug(key+" : "+actualMap.get(key));
						Object value = actualMap.get(key);
						jsonObject.put(key, value);
					}
					if(sendDataToRestUrl(jsonObject,actualMap))
					{
						insertCnt++;
					}
					else
					{
						insertCnt--;
					};
				}
				catch (Exception e)
				{
					LOGGER.error("Exception: "+e.getMessage());
					e.printStackTrace();
					insertCnt--;
				}
			}
    	}
    	catch (Exception e)
    	{
			LOGGER.error("WeNestIm WMS createStockMovement Error: " + e.getMessage());
			e.printStackTrace();
			insertCnt = -1;
		}
		LOGGER.info("actualCount : " + actualList.size() + ",insertCount : " + insertCnt);
		if( actualList.size() != insertCnt)
		{
			LOGGER.error("WeNestIm WMS createStockMovement Error Count : " + (actualList.size()-insertCnt));
		}
		LOGGER.info("WeNestIm WMS createStockMovement End");
    	return insertCnt;
    }

	public boolean sendDataToRestUrl(JSONObject outObject, HashMap dataMap)
    {

		BufferedReader     in  = null;
		OutputStreamWriter out = null;
		Boolean rtnBoolResult  = false;
		try
    	{
			String clientId    = outObject.getString("CLIENT_ID"   );
			String companyCode = outObject.getString("COMPANY_CODE");

			URL url = new URL(AppUtil.getHostAddress()+"/wms-rest/Inventory/StockMovement/Create/wenestim-wms-demon/"+clientId+"/"+companyCode+"/MES_IF");
			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(5000);
			connection.setReadTimeout(5000);

			out = new OutputStreamWriter(connection.getOutputStream());
			out.write("["+outObject.toString()+"]");
			out.close();

			String thisLine;
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			while ((thisLine = in.readLine()) != null)
			{
				JSONObject inObject = new JSONObject(thisLine);
				if( inObject.getInt("ret") == 0)
				{
					rtnBoolResult = true;
					if( dataMap.get("INSERT_FLAG").toString().equals("Y") )
					{
						AppUtil.dbManager.delete("movement.deleteMesActualData", dataMap);
					}
				}

			}
			in.close();
    	}
    	catch (Exception e)
    	{
			LOGGER.error("WeNestIm WMS sendDataToRestUrl Error: " + e.getMessage());
			e.printStackTrace();
		}
		LOGGER.info("WeNestIm WMS sendDataToRestUrl End");
    	return rtnBoolResult;
    }
}

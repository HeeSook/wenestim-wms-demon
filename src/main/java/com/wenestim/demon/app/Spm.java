package com.wenestim.demon.app;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.wenestim.util.AppUtil;

/**
 * @author Andrew
 *
 */
public class Spm
{
	private final static Logger LOGGER = Logger.getLogger(Spm.class);


	public Spm()
	{

	}

	public int createSpm()
    {
		LOGGER.info("WeNestIm WMS createSpm Start");
		List<HashMap<String,Object>> spmList = null;
		int i         = 0;
		int insertCnt = 0;
		try
    	{
			spmList   = AppUtil.dbManager.selectList("spm.getMesSpmList");
			for(HashMap<String,Object> spmMap : spmList)
			{
				try
				{
					i++;
					LOGGER.info("insert Index : "+i);
					HashMap<String,Object> dataMap = new HashMap<String,Object>();
					for ( String key : spmMap.keySet() )
					{
						LOGGER.debug(key+" : "+spmMap.get(key));
						Object value = spmMap.get(key);
						dataMap.put(key, value);
					}
					AppUtil.dbManager.delete("spm.deleteMesSpmData", dataMap);
					insertCnt++;
				}
				catch (Exception e)
				{
					LOGGER.error("Exception: "+e.getMessage());
					e.printStackTrace();
					insertCnt--;
				}
			}
    	}
    	catch (Exception e)
    	{
			LOGGER.error("WeNestIm WMS createSpm Error: " + e.getMessage());
			e.printStackTrace();
			insertCnt = -1;
		}
		LOGGER.info("spmCount : " + spmList.size() + ",insertCount : " + insertCnt);
		if( spmList.size() != insertCnt)
		{
			LOGGER.error("WeNestIm WMS createSpm Error Count : " + (spmList.size()-insertCnt));
		}

		LOGGER.info("WeNestIm WMS createSpm End");
    	return insertCnt;
    }
}

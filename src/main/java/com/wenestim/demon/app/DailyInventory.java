package com.wenestim.demon.app;

import java.util.Calendar;

import org.apache.log4j.Logger;

import com.wenestim.util.AppUtil;

/**
 * @author Andrew
 *
 */
public class DailyInventory
{
	private final static Logger LOGGER = Logger.getLogger(DailyInventory.class);


	public DailyInventory()
	{

	}

	public int createDailyInventory()
    {
		LOGGER.info("WeNestIm WMS Daily Inventory Start");
		Calendar calendar = Calendar.getInstance();
		int currHour      = calendar.get(Calendar.HOUR_OF_DAY);
		int insertCnt     = 0;

		LOGGER.debug("currHour : " + currHour);

		try
    	{
    		if ( currHour >= 6 && currHour <= 7)
    		{
	    		String  executeFlag = (String)AppUtil.dbManager.selectOne("inventory.checkInvExecuteFlag").get("INV_EXECUTE_FLAG");
	    		LOGGER.debug("executeFlag : " + executeFlag);

	    		if( executeFlag.equals("Y"))
	    		{
	    			insertCnt = AppUtil.dbManager.insert("inventory.insertDailyInventory");
	        		LOGGER.debug("insertCnt : " + insertCnt);
	    		}
    		}
    	}
    	catch (Exception e)
    	{
			LOGGER.error("WeNestIm WMS Daily Inventory Error: " + e.getMessage());
			e.printStackTrace();
			insertCnt = -1;
		}
		LOGGER.info("WeNestIm WMS Daily Inventory End");
    	return insertCnt;
    }

}

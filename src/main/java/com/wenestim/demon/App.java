package com.wenestim.demon;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.wenestim.demon.app.DailyInventory;
import com.wenestim.demon.app.Spm;
import com.wenestim.demon.app.StockMovement;
import com.wenestim.util.AppUtil;

/**
 * @author Andrew
 *
 */
public class App
{
	private final static Logger   LOGGER         = Logger.getLogger(App.class);
	private final static int      delayTime      = 5*60;
//	private final static int      interval       = 1;
	private static DailyInventory dailyInventory = new DailyInventory();
	private static StockMovement  stockMovement  = new StockMovement();
	private static Spm            spm            = new Spm();

	public static void main( String[] args)
    {
		LOGGER.info("WeNestIm WMS Demon Start");
    	try
    	{
    		AppUtil.setHostAddress(getServerIP());
    		startTimer();
    	}
    	catch (Exception e)
    	{
			LOGGER.error("WeNestIm WMS Start Failed: " + e.getMessage());
			e.printStackTrace();
		}
    }

	private static void startTimer()
	{
		try
		{
			for(;;)
			{
				start();
				TimeUnit.SECONDS.sleep(delayTime);
			}


//			Timer timer       = new Timer();
//			Calendar calendar = Calendar.getInstance();
//			int currMin       = calendar.get(Calendar.MINUTE);
//			@SuppressWarnings("unused")
//			int scMin = currMin;
//
//			// calibration to make even-odd number minute
//			scMin = currMin + (inteval - (currMin%inteval));
//
//			Calendar date = Calendar.getInstance();
//			date.set(Calendar.MINUTE, 0);
//			date.set(Calendar.SECOND, 0);
//			date.set(Calendar.MILLISECOND, 0);
//			timer.scheduleAtFixedRate(
//				new TimerTask() {
//					@Override
//					public void run()
//					{
//						if(isStarted)
//						{
//							LOGGER.info("Start Demon");
//							isStarted = false;
//							start();
//							isStarted = true;
//							LOGGER.info("End Demon");
//						}
//					}
//				}
//				, date.getTime(), 1000 * 60 * inteval);


		}
		catch (Exception e)
		{
			LOGGER.error("startTimer Exception: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void start()
	{
		try
		{
			dailyInventory.createDailyInventory();
			stockMovement.createStockMovement();
			spm.createSpm();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static String getServerIP()
	{
		InetAddress ip;
        String      hostAddress = "";
        try
        {
        	ip          = InetAddress.getLocalHost();
            hostAddress = "http://"+ip.getHostAddress()+":8080";

        	List<HashMap<String,Object>>confingList = AppUtil.dbManager.selectList("common.getConfigList");
    		for(int i=0; i<confingList.size(); i++)
    		{
    			HashMap configMap = confingList.get(i);
    			if ( configMap.get("KEY").equals("WMS_REST_URL") && !configMap.get("VALUE").equals("IP"))
    			{
    				hostAddress = "http://"+(String)configMap.get("VALUE");
    			}
    		}
        }
        catch (UnknownHostException e)
        {

            e.printStackTrace();
        }
        LOGGER.info("Your current IP address : " + hostAddress);

        return hostAddress;
	}
}

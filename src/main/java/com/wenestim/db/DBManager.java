package com.wenestim.db;

import java.io.IOException;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

import com.wenestim.util.AppUtil;

public class DBManager
{

	final static Logger LOGGER = Logger.getLogger(DBManager.class);

	private static SqlSessionFactory sessionFactory = null;
	private SqlSession sqlSession = null ;

	public DBManager()
	{
		setSessionFactory();
	}

	public static void setSessionFactory()
	{
		String resName  = null;
		Reader reader   = null;
		try
		{
			resName = AppUtil.getPropertyByName("MYBATIS_CONFIG")  ;
			reader  = Resources.getResourceAsReader(resName);
			sessionFactory = new SqlSessionFactoryBuilder().build(reader);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			LOGGER.error("Resources Reader Create Fail : " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			try
			{
				reader.close();
			}
			catch (IOException e)
			{
				LOGGER.error("Resources Reader Close Fail : " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	public static SqlSessionFactory getSessionFactory()
	{
		 return sessionFactory;
	}

	public HashMap<String,Object> selectOne( String queryKey )
	{
		return selectOne( queryKey, null );
	}

	public List<HashMap<String,Object>> selectList( String queryKey )
	{
		return selectList( queryKey, null );
	}

	public List<HashMap<String,Object>> selectList( String queryKey, HashMap<String,Object> queryMap )
	{
		List<HashMap<String,Object>> resultList = null;

		try
		{
			this.sqlSession = this.sessionFactory.openSession();
			resultList      = sqlSession.selectList( queryKey, queryMap );

		}
		catch(Exception e)
		{
			LOGGER.error("selectList Fail : " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			this.sqlSession.close();
		}

		return resultList;
	}


	public HashMap<String,Object> selectOne( String queryKey, HashMap<String,Object> queryMap )
	{
		HashMap<String,Object> resultMap = null;

		try
		{
			this.sqlSession = this.sessionFactory.openSession();
			resultMap      = sqlSession.selectOne( queryKey, queryMap );

		}
		catch(Exception e)
		{
			LOGGER.error("selectList Fail : " + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			this.sqlSession.close();
		}

		return resultMap;
	}



	public int insert( String queryKey )
	{
		return traction("insert", queryKey, null);
	}

	public int insert( String queryKey, HashMap<String,Object> queryMap )
	{
		return traction("insert", queryKey, queryMap);
	}

	public int update( String queryKey )
	{
		return update( queryKey, null );
	}

	public int update( String queryKey, HashMap<String,Object> queryMap )
	{
		return traction("delete", queryKey, queryMap);
	}

	public int delete( String queryKey )
	{
		return traction("delete", queryKey, null);
	}

	public int delete( String queryKey, HashMap<String,Object> queryMap )
	{
		return traction("delete", queryKey, queryMap);
	}

	private int traction( String queryKind,  String queryKey, HashMap<String,Object> queryMap )
	{
		int rtnCnt = 0    ;

		try
		{
			this.sqlSession = this.sessionFactory.openSession();
			if( queryKind.equalsIgnoreCase("insert"))
			{
				rtnCnt = sqlSession.insert( queryKey,queryMap );
			}
			else if( queryKind.equalsIgnoreCase("update"))
			{
				rtnCnt = sqlSession.update( queryKey,queryMap );
			}
			else if( queryKind.equalsIgnoreCase("delete"))
			{
				rtnCnt = sqlSession.delete( queryKey,queryMap );
			}

		}
		catch(Exception e)
		{
			LOGGER.error( queryKind + " Fail : " + e.getMessage());
			e.printStackTrace();
			rtnCnt = -1;
		}
		finally
		{
			sqlSession.commit();
			sqlSession.close();
		}
		return rtnCnt;
	}
}
